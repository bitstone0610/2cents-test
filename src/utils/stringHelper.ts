export const getShortAddr = (addr: string) =>
  `${addr.slice(0, 4)}...${addr.slice(38)}`;

export const getFormatedDate = (date: Date) => {
  return (
    date.toLocaleString("en-us", {
      month: "short",
    }) +
    date.toLocaleString("en-us", {
      day: "numeric",
    })
  );
};
