export interface PostItem {
  id: string;
  attributes: {
    posterUserName: string;
    postImg: string;
    upvotes: string[];
    postTxt: string;
    posterAcc: string;
    createdAt: Date;
    ethAddress: string;
    posterPfp: string;
  };
}
