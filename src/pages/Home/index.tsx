import { useState, createRef } from "react";
import { Icon } from "web3uikit";
import "./styles.css";
import { defaultImgs } from "../../utils/constants";
import { PostInFeed } from "../../components";
import { useMoralis } from "react-moralis";

const Home = () => {
  const { Moralis } = useMoralis();
  const user = Moralis.User.current();
  const inputFile = createRef<HTMLInputElement>();
  const [selectedFile, setSelectedFile] = useState<any>();
  const [theFile, setTheFile] = useState<any>();
  const [post, setPost] = useState<string>();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const onImageClick = () => {
    inputFile.current?.click();
  };

  const changeHandler = (event: any) => {
    const img = event.target.files[0];
    setTheFile(img);
    setSelectedFile(window.URL.createObjectURL(img));
  };

  async function savePost() {
    if (!post) return;
    const Posts = Moralis.Object.extend("Posts");

    const newPost = new Posts();
    newPost.set("postTxt", post);
    newPost.set("posterPfp", user?.attributes?.pfp);
    newPost.set("posterAcc", user?.attributes?.ethAddress);
    newPost.set("posterUserName", user?.attributes?.username);

    if (theFile) {
      const data = theFile;
      const file: any = new Moralis.File(data.name, data);
      await file.saveIPFS();
      newPost.set("postImg", file.ipfs());
    }
    setIsLoading(true);
    await newPost.save();
    window.location.reload();
  }
  return (
    <>
      <div className="mainContent">
        <div className="profilePost">
          <img src={defaultImgs[0]} className="profilePic" />
          <div className="postBox">
            <textarea value={post} onChange={(e) => setPost(e.target.value)} />
            {selectedFile && <img src={selectedFile} className="postImg"></img>}
            <div className="imgOrPost">
              <div className="imgDiv" onClick={onImageClick}>
                <input
                  type="file"
                  name="file"
                  style={{ display: "none" }}
                  ref={inputFile}
                  onChange={changeHandler}
                />
                <Icon fill="#61dafb" size={20} svg="image"></Icon>
              </div>
              <div className="postOptions">
                <div className="post" onClick={savePost}>
                  POST
                </div>
                <div className="matic">
                  <Icon fill=" #61dafb" size={20} svg="matic" />
                </div>
              </div>
            </div>
          </div>
        </div>
        {isLoading && <div className="loading">Loading ...</div>}
        <PostInFeed profile={false} />
      </div>
    </>
  );
};

export default Home;
