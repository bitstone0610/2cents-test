import { Input } from "web3uikit";
import "./styles.css";
import spaceshooter from "../../images/spaceshooter.jpeg";
import netflix from "../../images/netflix.jpeg";
import academy from "../../images/academy.png";
import youtube from "../../images/youtube.png";
import js from "../../images/js.png";

const Rightbar = () => {
  const trends = [
    {
      img: spaceshooter,
      text: "Bored Ape",
    },
    {
      img: netflix,
      text: "Andrew",
    },
    {
      img: academy,
      text: "Frost",
    },
    {
      img: js,
      text: "Apollo",
    },
    {
      img: youtube,
      text: "MonkeyKing",
    },
  ];

  return (
    <>
      <div className="rightbarContent">
        <Input
          label="Search 2Cents"
          name="Search 2Cents"
          prefixIcon="search"
          labelBgColor="#141d26"
        />
        <div className="trends">
          <strong>LeaderBoard</strong> ($CEN Staked)
          {trends.map((e) => {
            return (
              <>
                <div className="trend">
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <img src={e.img} className="trendImg"></img>
                    <div className="trendText">{e.text}</div>
                  </div>

                  <div className="trendText">
                    <strong>15.22</strong> $CEN
                  </div>
                </div>
              </>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Rightbar;
