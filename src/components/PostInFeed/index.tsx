import { useEffect, useState } from "react";
import { defaultImgs } from "../../utils/constants";
import { getShortAddr, getFormatedDate } from "../../utils/stringHelper";
import { PostItem } from "../../utils/types";
import { Icon } from "web3uikit";
import { useMoralis } from "react-moralis";

import "./styles.css";

const PostInFeed = ({ profile }: { profile: boolean }) => {
  const [postArr, setPostArr] = useState<any[]>([]);
  const { Moralis, account } = useMoralis();
  const user = Moralis.User.current();

  useEffect(() => {
    async function getPosts() {
      await queryPosts();
    }
    getPosts();
  }, [profile]);

  async function queryPosts() {
    try {
      const Posts = Moralis.Object.extend("Posts");
      const query = new Moralis.Query(Posts);

      let subscription = await query.subscribe();
      subscription.on("create", () => {
        queryPosts();
      });
      subscription.on("update", () => {
        queryPosts();
      });

      if (profile) {
        query.equalTo("posterAcc", account);
      }

      const results = await query.find();
      setPostArr(results);
    } catch (error) {}
  }

  async function toggleUpvote(postId: string, currentUpvotes: string[]) {
    const Posts = Moralis.Object.extend("Posts");
    const query = new Moralis.Query(Posts);
    const authUserEthAddress = user?.attributes.ethAddress;

    query.equalTo("objectId", postId);
    let results = await query.find();
    if (results && results[0]) {
      if (currentUpvotes.includes(authUserEthAddress)) {
        results[0].remove("upvotes", authUserEthAddress);
      } else {
        results[0].add("upvotes", authUserEthAddress);
      }
      await results[0].save();
      await queryPosts();
    }
  }

  return (
    <>
      {postArr
        ?.map((e: PostItem) => {
          const {
            ethAddress,
            posterUserName,
            posterPfp,
            posterAcc,
            upvotes,
            createdAt,
            postTxt,
            postImg,
          } = e.attributes;
          return (
            <>
              <div className="feedPost">
                <img src={posterPfp || defaultImgs[0]} className="profilePic" />
                <div className="completePost">
                  <div className="who">
                    {posterUserName.slice(0, 6)}
                    <div className="accWhen">
                      {`${getShortAddr(posterAcc)} · ${getFormatedDate(
                        createdAt
                      )}`}
                    </div>
                  </div>
                  <div className="postContent">
                    {postTxt}
                    {postImg && <img src={postImg} className="postImg" />}
                  </div>
                  <div className="interactions">
                    <div className="interactionNums">
                      <Icon fill="#3f3f3f" size={20} svg="messageCircle" />
                      20
                    </div>
                    <div
                      className="interactionNums"
                      onClick={() => toggleUpvote(e.id, upvotes || [])}
                    >
                      <Icon
                        fill={
                          upvotes && upvotes.includes(ethAddress)
                            ? "#61dafb"
                            : "#3f3f3f"
                        }
                        size={20}
                        svg="star"
                      />
                      {upvotes?.length || 0}
                    </div>
                    <div className="interactionNums">
                      <Icon fill="#3f3f3f" size={20} svg="matic" />
                      12
                    </div>
                  </div>
                </div>
              </div>
            </>
          );
        })
        .reverse()}
    </>
  );
};

export default PostInFeed;
